﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SYSBO.STR;

namespace SYSBO.CONECT
{
    public class conexion
    {
        public obj_return_values ejecutarSQL(string sentencia)
        {
            obj_return_values OBJreturn = new obj_return_values();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand(sentencia, cn);
            string cadenaconx = CadenadeConexion();
            SqlDataAdapter da;
            DataTable dt = new DataTable();
            util ut = new util();
            string catch_error = "";
            try
            {
                cn.ConnectionString = cadenaconx;

                if (cn != null && cn.State != ConnectionState.Closed) { cn.Close(); }

                cn.Open();
                cmd.CommandTimeout = 180;
                da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                //cn.Close();
                OBJreturn.resultset = dt;
            }
            catch (Exception e)
            {
                OBJreturn.error_servicio = DateTime.Now.ToString() + ".  Error en conexion sql [conexionSQL]: " + sentencia + ".  " + e.Message;
                catch_error = e.Message;
                ut.bitacora(DateTime.Now.ToString() + "   Error resultado SQL, [conexionSQL] " + sentencia + ".  " + catch_error.Substring(1, 100), 0);
            }
            finally
            {
                if (cn != null && cn.State != ConnectionState.Closed) { cn.Close(); }
            }
            return OBJreturn;

        }
        private static string CadenadeConexion()
        {
            string Cadenade = "Server=192.168.1.11;Database=aduasism3_new;UID=devwms;PWD=sa;Pooling=False;";
            return Cadenade;
        }
    }
}

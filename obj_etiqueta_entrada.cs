﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SYSBO
{
    public class obj_etiqueta_entrada
    {
        public string entrada { get; set; }
        public string lote { get; set; }
        public string bulto { get; set; }
        public string cantidad { get; set; }
        public string fecha_entrada { get; set; }
        public string seccion { get; set; }
        public string guia { get; set; }
        public string num_vehiculo { get; set; }
        public string linea_fletera { get; set; }
        public string agencia_aduanal { get; set; }
        public string forwarding { get; set; }
        public string ciudad { get; set; }
        public string estado { get; set; }
        public string observaciones { get; set; }
        public string haz_mat { get; set; }
        public string ref_ups { get; set; }

        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }

        public obj_etiqueta_entrada()
        {
            entrada = "NO DATA";
            lote = "NO DATA";
            bulto = "NO DATA";
            cantidad = "NO DATA";
            fecha_entrada = "NO DATA";
            seccion = "NO DATA";
            guia = "NO DATA";
            num_vehiculo = "NO DATA";
            linea_fletera = "NO DATA";
            agencia_aduanal = "NO DATA";
            forwarding = "NO DATA";
            ciudad = "NO DATA";
            estado = "NO DATA";
            observaciones = "NO DATA";
            haz_mat = "NO DATA";
            ref_ups = "NO DATA";
            ErrorNumber = "NO DATA";
            ErrorMessage = "NO DATA";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SYSBO
{
    public class obj_entradas
    {
        public string entrada { get; set; }
        public string bodega { get; set; }
        public string cliente { get; set; }
        public string cantidad { get; set; }
        public string guia_flete { get; set; }
        public string ref_ups { get; set; }
        public string seccion { get; set; }
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
        public string obs { get; set; }

        public obj_entradas()
        {
            entrada = "NO DATA";
            bodega = "NO DATA";
            cliente = "NO DATA";
            cantidad = "NO DATA";
            guia_flete = "NO DATA";
            ref_ups = "NO DATA";
            seccion = "NO DATA";
            ErrorNumber = "NO DATA";
            ErrorMessage = "NO DATA";
            obs = "NO DATA";
        }
    }
}
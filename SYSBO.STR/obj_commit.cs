﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SYSBO.STR
{
    public class obj_commit
    {
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
        public string obs { get; set; }

        public obj_commit()
        {
            ErrorNumber = "NO DATA";
            ErrorMessage = "NO DATA";
            obs = "NO DATA";   
        }
    }
}

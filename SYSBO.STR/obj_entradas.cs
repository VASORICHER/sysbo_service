﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SYSBO.STR
{
    public class obj_entradas
    {
        public string id_entrada { get; set; }
        public string id_bodega { get; set; }        
        public string id_cliente { get; set; }        
        public string cantidad { get; set; }        
        public string ref_ups { get; set; }        
        public string guia_flete { get; set; }        
        public string seccion { get; set; }        
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
        public obj_entradas()
        {
            id_entrada = "NO DATA";
            id_bodega = "NO DATA";            
            id_cliente = "NO DATA";  
            cantidad = "NO DATA";  
            ref_ups = "NO DATA";  
            guia_flete = "NO DATA";
            seccion = "NO DATA";  
            ErrorNumber = "NO DATA";
            ErrorMessage = "NO DATA";            
        }  
    }
}

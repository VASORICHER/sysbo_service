﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace SYSBO
{
    public class cl_clientes
    {
        public obj_return_values listaBodegas(string par_bodega)
        {

            DataTable dt = new DataTable();
            conexion conecta = new conexion();
            obj_return_values OBJreturn = new obj_return_values();
            util ut = new util();
            string catch_error = "";
            try
            {
                OBJreturn = conecta.ejecutarSQL("exec er_SYSBO_cata_clientes '"+par_bodega+"'");
            }
            catch (Exception e)
            {
                OBJreturn.error_servicio = DateTime.Now.ToString() + ".   Error resultado SQL, [cl_clientes]: " + e.Message;
                catch_error = e.Message;
                ut.bitacora(DateTime.Now.ToString() + "   Error resultado SQL, [cl_clientes] " + catch_error.Substring(1, 100), 0);
            }
            return OBJreturn;
        } 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SYSBO
{
    public class obj_clientes
    {
        public string id_cliente { get; set; }
        public string razon_social { get; set; }
        public string rfc { get; set; }
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
        public obj_clientes()
        {
            id_cliente = "NO DATA";
            razon_social = "NO DATA";
            rfc = "NO DATA";
            ErrorNumber = "NO DATA";
            ErrorMessage = "NO DATA";
        } 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace SYSBO
{
    public class cl_gen_entrada
    {
        public obj_return_values gen_entrada()
        {
            DataTable dt = new DataTable();
            conexion conn = new conexion();
            obj_return_values OBJreturn = new obj_return_values();
            util ut = new util();
            string catch_error = string.Empty;
            try
            {
                OBJreturn = conn.ejecutarSQL("er_SYSBO_gen_entrada");
            }
            catch (Exception ex)
            {
                OBJreturn.error_servicio = DateTime.Now.ToString() + ". Error resultado SQL, [cl_gen_entrada]: " + ex.Message;
                catch_error = ex.Message;
                ut.bitacora(DateTime.Now.ToString() + " Error resultado SQL, [cl_gen_entrada] " + catch_error.Substring(1, 100), 0);
            }
            return OBJreturn;
        }
    }
}
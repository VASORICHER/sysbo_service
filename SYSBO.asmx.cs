﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Configuration;
using SYSBO.Tools.Printer;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace SYSBO
{
    /// <summary>
    /// Summary description for SYSBO
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class SYSBO : System.Web.Services.WebService
    {

        //[WebMethod]
        //public string HelloWorld()
        //{
        //    return "Hello World";
        //}
    

    #region CATALOGOS

        [WebMethod(Description = "Catalogo de bodegas")]
        public List<obj_bodegas> catalogo_bodegas()
        {
            List<obj_bodegas> bodegas = new List<obj_bodegas>();
            cl_bodegas DA_bodegas = new cl_bodegas();
            DataTable dt = new DataTable();
            obj_return_values OBJreturn = new obj_return_values();
            OBJreturn = DA_bodegas.listaBodegas();
            if (OBJreturn.error_servicio != "NO DATA")
            {
                obj_bodegas unaBodega = new obj_bodegas();
                unaBodega.ErrorNumber = "1";
                unaBodega.ErrorMessage = "Error Metodo: " + OBJreturn.error_servicio;
                bodegas.Add(unaBodega);
                return bodegas;
            }
            dt = OBJreturn.resultset;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow objdataRow in dt.Rows)
                {
                    obj_bodegas unaBodega = new obj_bodegas();
                    unaBodega.id_bodega = objdataRow["id_bodega"].ToString();
                    unaBodega.razon_social = objdataRow["razon_social"].ToString();
                    bodegas.Add(unaBodega);
                }
            }
            else
            {
                obj_bodegas unaBodega = new obj_bodegas();
                unaBodega.ErrorNumber = "1";
                unaBodega.ErrorMessage = "Error Metodo: No existen bodegas";
                bodegas.Add(unaBodega);
            }
            return bodegas;
        }

        [WebMethod(Description = "Catalogo de clientes")]
        public List<obj_clientes> catalogo_clientes( string par_bodega)
        {
            List<obj_clientes> clientes = new List<obj_clientes>();
            cl_clientes DA_clientes = new cl_clientes();
            DataTable dt = new DataTable();
            obj_return_values OBJreturn = new obj_return_values();
            OBJreturn = DA_clientes.listaBodegas(par_bodega);
            if (OBJreturn.error_servicio != "NO DATA")
            {
                obj_clientes uncliente = new obj_clientes();
                uncliente.ErrorNumber = "1";
                uncliente.ErrorMessage = "Error Metodo: " + OBJreturn.error_servicio;
                clientes.Add(uncliente);
                return clientes;
            }
            dt = OBJreturn.resultset;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow objdataRow in dt.Rows)
                {
                    obj_clientes uncliente = new obj_clientes();
                    uncliente.id_cliente = objdataRow["id_cliente"].ToString();
                    uncliente.razon_social = objdataRow["razon_social"].ToString();
                    uncliente.rfc = objdataRow["rfc"].ToString();
                    clientes.Add(uncliente);
                }
            }
            else
            {
                obj_clientes uncliente = new obj_clientes();
                uncliente.ErrorNumber = "1";
                uncliente.ErrorMessage = "Error Metodo: No existen clientes";
                clientes.Add(uncliente);
            }
            return clientes;
        }


    #endregion


        [WebMethod(Description = "Registrar o actualizar entrada de bodega")]
        public obj_commit abc_entrada(string par_entrada, string par_cliente, string par_usuario, string par_bodega, string par_bultos, string par_guia, string par_ref, string par_seccion)
        {
            obj_commit commit = new obj_commit();
            cl_abc_entradas DA = new cl_abc_entradas();
            DataTable dt = new DataTable();
            cl_valida_dato validaDato = new cl_valida_dato();
            obj_return_values OBJreturn = new obj_return_values();
           
            if (validaDato.IsNumeric(par_bultos) == false)
            {
                commit.ErrorNumber = "1";
                commit.ErrorMessage = "Error Metodo: parametro cantidad de bultos " + par_bultos + " no es numerico";
                return commit;
            }          

            OBJreturn = DA.abc_entradas(par_entrada, par_cliente,par_usuario, par_bodega, Convert.ToInt32(par_bultos), par_guia, par_ref, par_seccion);
            if (OBJreturn.error_servicio != "NO DATA")
            {
               commit.ErrorNumber = "1";                        
               commit.ErrorMessage = "Error Metodo: " + OBJreturn.error_servicio;
               return commit;
            }
            dt = OBJreturn.resultset;
            if (dt.Rows.Count > 0)
            {
               foreach (DataRow objdataRow in dt.Rows)
               {
                 commit.entrada=objdataRow["entrada"].ToString();
                 if (objdataRow["ErrorNumber"] != DBNull.Value) { commit.ErrorNumber = objdataRow["ErrorNumber"].ToString(); }
                 if (objdataRow["ErrorMessage"] != DBNull.Value) { commit.ErrorMessage = objdataRow["ErrorMessage"].ToString(); }
                 commit.obs = "SP ersp_wms_abc_detalle_descarga,xxxxxxxxxxxxx insert";
               }
            }                         
            return commit;
        }

        [WebMethod(Description = "Informacion general de la entrada a consulta")]
        public List<obj_entradas> informacion_entrada(string par_id_entrada)
        {
            cl_rpt_entradas DA_entradas = new cl_rpt_entradas();
            List<obj_entradas> lista = new List<obj_entradas>();
            DataTable dt = new DataTable();
            obj_return_values OBJreturn = new obj_return_values();
            OBJreturn = DA_entradas.rpt_entrada(par_id_entrada);

            if (OBJreturn.error_servicio != "NO DATA")
            {
                obj_entradas rpt = new obj_entradas();
                rpt.ErrorNumber = "1";
                rpt.ErrorMessage = "Error Metodo: " + OBJreturn.error_servicio;
                lista.Add(rpt);
                return lista;
            }
            dt = OBJreturn.resultset;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow objdataRow in dt.Rows)
                {
                    obj_entradas rpt = new obj_entradas();
                    rpt.entrada = objdataRow["entrada"].ToString();
                    rpt.bodega = objdataRow["bodega"].ToString();
                    rpt.cliente = objdataRow["cliente"].ToString();
                    rpt.cantidad = objdataRow["cantidad"].ToString();
                    rpt.guia_flete = objdataRow["guia_flete"].ToString();
                    rpt.ref_ups = objdataRow["ref_ups"].ToString();
                    rpt.seccion = objdataRow["seccion"].ToString();                 
                    lista.Add(rpt);
                }
            }
            else
            {
                obj_entradas rpt = new obj_entradas();
                rpt.ErrorNumber = "1";
                rpt.ErrorMessage = "Error Metodo: Atencion, NO hay informacion en BD. Valores recibidos (" + par_id_entrada + ")";
                lista.Add(rpt);
            }
            return lista;
        }                     
        
        [WebMethod(Description ="Obtener una entrada nueva")]
        public obj_commit obtener_entrada()
        {
            obj_commit commit = new obj_commit();
            cl_gen_entrada DA_entradas = new cl_gen_entrada();
            DataTable dt = new DataTable();
            obj_return_values OBJreturn = new obj_return_values();

            OBJreturn = DA_entradas.gen_entrada();

            if (OBJreturn.error_servicio != "NO DATA")
            {
                commit.ErrorNumber = "1";
                commit.ErrorMessage = "Error Método: " + OBJreturn.error_servicio;
                return commit;
            }
            dt = OBJreturn.resultset;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    commit.entrada = row["Entrada"].ToString();
                }
            }
            else
            {
                obj_commit rpt = new obj_commit()
                {
                    ErrorNumber = "1",
                    ErrorMessage = "Error Método: Atención, ocurrió un error al obtener entrada."
                };
            }

            return commit;
        }

        [WebMethod]
        public obj_commit ObtenerEtiqueta(string entrada)
        {
            //CrystalReport2 etiquetaReporte = new CrystalReport2();
            //string reportPath = Server.MapPath("~/CrystalReport2.rpt");
            //etiquetaReporte.SetParameterValue("entrada", entrada);
            //etiquetaReporte.Load(reportPath);

            CrystalReport2 etiquetaReporte = new CrystalReport2();
            string reportPath = Server.MapPath("~/CrystalReport2.rpt");
            etiquetaReporte.Load(reportPath);
            //etiquetaReporte.SetParameterValue("entrada", entrada);


            if(etiquetaReporte.DataDefinition.ParameterFields.Count > 0)
            {
                foreach (ParameterFieldDefinition item in etiquetaReporte.DataDefinition.ParameterFields)
                {
                    if(item.ReportName == string.Empty)
                    {
                        object value = entrada;
                        etiquetaReporte.SetParameterValue(item.ParameterFieldName, value);
                    }
                }
            }
            ConnectionInfo crConn = new ConnectionInfo();
            crConn.ServerName = "192.168.1.11";
            crConn.DatabaseName = "aduasism3_new";
            crConn.UserID = "sa";
            crConn.Password = "sa";

            Tables crTables = etiquetaReporte.Database.Tables;
            for (int i = 0; i < crTables.Count; i++)
            {
                Table crTable = crTables[i];
                TableLogOnInfo tblInfo = crTable.LogOnInfo;
                tblInfo.ConnectionInfo = crConn;
                crTable.ApplyLogOnInfo(tblInfo);
            }
            /*var exportOptions = etiquetaReporte.ExportOptions;
            exportOptions.ExportDestinationType = ExportDestinationType.NoDestination;
            exportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
            var req = new ExportRequestContext { ExportInfo = exportOptions };
            var stream = etiquetaReporte.FormatEngine.ExportToStream(req);*/

            //HP SISTEMAS 2.55
            etiquetaReporte.PrintOptions.PrinterName = "PDFCreator";
            etiquetaReporte.PrintToPrinter(1, true, 1, 1);





            var commit = new obj_commit();
            DataTable dt = new DataTable();
            obj_etiqueta_entrada objeto = new obj_etiqueta_entrada();
            
            var cl_etiqueta = new EtiquetaEntrada();
            string folder_digial = ConfigurationManager.AppSettings["folder_digital"];

            var etiqueta = cl_etiqueta.CrearEtiqueta(entrada);
            dt = etiqueta.resultset;
            
            if(dt.Rows.Count>0)
            {
                foreach (DataRow row in dt.Rows)
                {

                    objeto.entrada = row["id_entrada"].ToString();
                    objeto.lote = row["lote"].ToString();
                    objeto.bulto = row["bulto"].ToString();
                    objeto.cantidad = row["cantidad"].ToString();
                    objeto.fecha_entrada = row["fecha_entrada"].ToString();
                    objeto.seccion = row["seccion_entrada"].ToString();
                    objeto.guia = row["guia_flete"].ToString();
                    objeto.forwarding = row["forwarding"].ToString();
                    objeto.linea_fletera = row["linea_fletera"].ToString();
                    objeto.agencia_aduanal = row["agencia_aduanal"].ToString();
                    objeto.ciudad = row["ciudad"].ToString();
                    objeto.estado = row["estado"].ToString();
                    objeto.haz_mat = row["haz_mat"].ToString();
                    objeto.num_vehiculo = row["num_vehiculo"].ToString();
                    objeto.observaciones = row["observaciones"].ToString();
                    objeto.ref_ups = row["ref"].ToString();
                }
                var fileName = Diamond.CreateDiamondPDF(objeto);
            }
            else
            {
                obj_commit rpt = new obj_commit()
                {
                    ErrorNumber = "1",
                    ErrorMessage = "Error Método: Atención, ocurrió un error al generar etiqueta."
                };
            }

            return commit;
        }

        [WebMethod]
        public List<string> ObtenerImpresoras() => PrinterSettings.InstalledPrinters.Cast<string>().ToList();
    }
}

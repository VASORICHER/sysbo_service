﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace SYSBO
{
    public class obj_return_values
    {
        public DataTable resultset { get; set; }
        public string error_servicio { get; set; }

        public obj_return_values()
        {
            error_servicio = "NO DATA";
        }
    }
}
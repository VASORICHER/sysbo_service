﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SYSBO
{
    public class EtiquetaEntrada
    {

        public obj_return_values CrearEtiqueta(string entrada)
        {
            obj_return_values etiqueta = new obj_return_values();
            DataTable dt = new DataTable();
            conexion conn = new conexion();
            util ut = new util();
            string catch_error = string.Empty;
            //er_SYSBO_etiqueta
            try
            {
                etiqueta = conn.ejecutarSQL($"er_SYSBO_etiqueta '{entrada}'");
            }
            catch (Exception ex)
            {
                etiqueta.error_servicio = $"{DateTime.Now.ToString()}. Error resultado SQL, [EtiquetEntrada]: {ex.Message}";
                catch_error = ex.Message;
                ut.bitacora($"{DateTime.Now.ToString()}. Error resultado SQL, [EtiquetaEntrada] {catch_error.Substring(1, 100)}", 0);
            }

            return etiqueta;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SYSBO
{
    public class obj_bodegas
    {
        public string id_bodega { get; set; }
        public string razon_social { get; set; }
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
        public obj_bodegas()
        {
            id_bodega = "NO DATA";
            razon_social = "NO DATA";
            ErrorNumber = "NO DATA";
            ErrorMessage = "NO DATA";
        }  
    }
}
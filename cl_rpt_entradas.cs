﻿using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace SYSBO
{
    public class cl_rpt_entradas
    {
        public obj_return_values rpt_entrada(string par_id_entrada)
        {
            DataTable dt = new DataTable();
            conexion conecta = new conexion();
            obj_return_values OBJreturn = new obj_return_values();
            util ut = new util();
            string catch_error = "";
            try
            {
                OBJreturn = conecta.ejecutarSQL("er_SYSBO_info_entrada '" + par_id_entrada + "'");
            }
            catch (Exception e)
            {
                OBJreturn.error_servicio = DateTime.Now.ToString() + ".   Error resultado SQL, [cl_rpt_entradas]: " + e.Message;
                catch_error = e.Message;
                ut.bitacora(DateTime.Now.ToString() + "   Error resultado SQL, [cl_rpt_entradas] " + catch_error.Substring(1, 100), 0);
            }
            return OBJreturn;
        }       
    }
}
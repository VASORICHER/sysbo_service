﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SYSBO
{
    public class obj_rpt_entradas
    {
        public string entrada { get; set; }
        public string bodega { get; set; }
        public string cliente { get; set; }
        public string cantidad { get; set; }
        public string ref_ups { get; set; }
        public string guia_flete { get; set; }
        public string seccion { get; set; }
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
        public obj_rpt_entradas()
        {
            entrada = "NO DATA";
            bodega = "NO DATA";
            cliente = "NO DATA";
            cantidad = "NO DATA";
            ref_ups = "NO DATA";
            guia_flete = "NO DATA";
            seccion = "NO DATA";
            ErrorNumber = "NO DATA";
            ErrorMessage = "NO DATA";
        }  
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SYSBO.STR;
using SYSBO.CONECT;

namespace SYSBO.EXEC
{
    public class cl_bodegas
    {        
        public obj_return_values listaBodegas()
        {

            DataTable dt = new DataTable();
            conexion conecta = new conexion();
            obj_return_values OBJreturn = new obj_return_values();
            util ut = new util();
            string catch_error = "";
            try
            {
                OBJreturn = conecta.ejecutarSQL("exec er_SYSBO_cata_bodegas ");
            }
            catch (Exception e)
            {
                OBJreturn.error_servicio = DateTime.Now.ToString() + ".   Error resultado SQL, [CLbodegas]: " + e.Message;
                catch_error = e.Message;
                ut.bitacora(DateTime.Now.ToString() + "   Error resultado SQL, [CLbodegas] " + catch_error.Substring(1, 100), 0);
            }
            return OBJreturn;           
        } 
    }
}

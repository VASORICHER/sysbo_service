﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace SYSBO
{
    public class cl_abc_entradas
    {
        public obj_return_values abc_entradas(string par_entrada, string par_cliente, string par_usuario, string par_bodega, int par_bultos, string par_guia, string par_ref, string par_seccion)
        {            		
            DataTable dt = new DataTable();
            conexion conecta = new conexion();
            obj_return_values objreturn = new obj_return_values();
            util ut = new util();
            string catch_error = "";
            try
            {
                objreturn = conecta.ejecutarSQL("er_SYSBO_abc_entradaBodega '" + par_entrada + "','" + par_cliente + "','" + par_usuario + "','" + par_bodega + "'," + par_bultos + ",'" + par_guia + "','" + par_ref + "','" + par_seccion + "'");
            }
            catch (Exception e)
            {
                objreturn.error_servicio = DateTime.Now.ToString() + ".   Error resultado SQL, [cl_abc_entradas]: " + e.Message;
                catch_error = e.Message;
                ut.bitacora(DateTime.Now.ToString() + "   Error resultado SQL, [cl_abc_entradas] " + catch_error.Substring(1, 100), 0);
            }
            return objreturn;
        }        
    }
}
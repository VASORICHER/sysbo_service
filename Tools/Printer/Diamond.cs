﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SYSBO.Tools.Printer
{
    public class Diamond
    {
        internal static string CreateDiamondPDF(obj_etiqueta_entrada _etiquetaEntrada)
        {
            string _directoryTarget = Path.GetTempPath() + "Generador\\";
            string nameFile = _directoryTarget + Guid.NewGuid().ToString().Replace("-", "") + ".pdf";

            if (!Directory.Exists(_directoryTarget))
                Directory.CreateDirectory(_directoryTarget);
            if (File.Exists(nameFile))
                File.Delete(nameFile);

            var document = new Document(new Rectangle(100,216), 10f, 10f, 10f, 0f);
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            var file = new FileStream(nameFile, FileMode.OpenOrCreate);

            try
            {
                var writer = PdfWriter.GetInstance(document, file);
                PdfContentByte cb = new PdfContentByte(writer);
                document.Open();
                bool ShowBorderDebug = false;
                bool ShowBorderRelease = true;

                iTextSharp.text.pdf.Barcode39 barcode39 = new Barcode39();
                barcode39.Code = _etiquetaEntrada.entrada + "-" + _etiquetaEntrada.bulto;
                barcode39.Font = BaseFont.CreateFont("Courier-Bold", "winansi", false);
                barcode39.StartStopText = false;
                barcode39.Extended = false;
                barcode39.BarHeight = 100.0f;
                barcode39.Size = 45f;
                barcode39.N = 6.8f;
                barcode39.Baseline = 32f;
                barcode39.X = 1.1f;
                iTextSharp.text.Image image39 = barcode39.CreateImageWithBarcode(cb, null, null);

                //Agencia Aduanal
                var lsAgenciaAduanal = new List<PdfPCell>();
                var dcAgenciaAduanal = new DiamondCell($"{_etiquetaEntrada.agencia_aduanal}/{_etiquetaEntrada.forwarding}", Element.ALIGN_JUSTIFIED, FontType.TituloBold, ShowBorderDebug);
                dcAgenciaAduanal.PaddingLeft = 15;
                lsAgenciaAduanal.Add(dcAgenciaAduanal);
                document.Add(new DiamondTable(lsAgenciaAduanal));


                //LAREDO, TX
                var lsCiudad = new List<PdfPCell>();
                var dcCiudad = new DiamondCell($"{_etiquetaEntrada.ciudad},{_etiquetaEntrada.estado}", Element.ALIGN_LEFT, FontType.Normal, ShowBorderDebug);
                dcCiudad.PaddingLeft = 166;
                lsCiudad.Add(dcCiudad);
                var tblCiudad = new DiamondTable(lsCiudad);
                document.Add(tblCiudad);


                //REGISTRO DE ENTRADA
                var lsRegistroEntrada = new List<PdfPCell>();
                var dcRegistroEntrada = new DiamondCell($"REGISTRO DE ENTRADA", Element.ALIGN_LEFT, FontType.Normal, ShowBorderDebug);
                dcRegistroEntrada.PaddingLeft = 130;
                lsRegistroEntrada.Add(dcRegistroEntrada);
                var tblRegistroEntrada = new DiamondTable(lsRegistroEntrada);
                document.Add(tblRegistroEntrada);

                //Agregamos el codigo de barras                
                document.Add(image39);

                document.Add(new DiamondTable(new List<PdfPCell>()
                {
                     new DiamondCell("Total Bultos:", Element.ALIGN_LEFT, FontType.NormalBold,ShowBorderDebug),
                     new DiamondCell(_etiquetaEntrada.bulto, Element.ALIGN_LEFT, FontType.Normal,ShowBorderDebug)
                }, new float[] { 67, 524 }));
                document.Add(new DiamondTable(new List<PdfPCell>()
                {
                     new DiamondCell("HAZ-MAT:", Element.ALIGN_LEFT, FontType.NormalBold,ShowBorderDebug),
                     new DiamondCell(_etiquetaEntrada.haz_mat, Element.ALIGN_LEFT, FontType.Normal,ShowBorderDebug)
                }, new float[] { 67, 524 }));
                document.Add(new DiamondTable(new List<PdfPCell>()
                {
                     new DiamondCell("No Caja/Vehiculo:", Element.ALIGN_LEFT, FontType.NormalBold,ShowBorderDebug),
                     new DiamondCell(_etiquetaEntrada.num_vehiculo, Element.ALIGN_LEFT, FontType.Normal,ShowBorderDebug)
                }, new float[] { 67, 524 }));

                //var cellLinea = new DiamondCell("Linea:", Element.ALIGN_LEFT, FontType.NormalBold, ShowBorderDebug);
                //var cellLineaValor = new DiamondCell(_etiquetaEntrada.linea_fletera, Element.ALIGN_LEFT, FontType.Normal, ShowBorderDebug);
                //var cellFb = new DiamondCell("F.B.No.:", Element.ALIGN_RIGHT, FontType.NormalBold, ShowBorderDebug);
                //var cellFbValor = new DiamondCell(_etiquetaEntrada.guia, Element.ALIGN_RIGHT, FontType.Normal, ShowBorderDebug);                                                          
                //cellLinea.PaddingRight = 50;
                //cellLineaValor.PaddingRight = 50;
                //cellFb.PaddingRight = 50;
                //cellFbValor.PaddingRight = 50;
                //var cells = new DiamondCell[]
                //{
                //    cellLinea,cellLineaValor, cellFb,cellFbValor
                //};                
                //var tblCells = new DiamondTable(cells);
                //document.Add(tblCells);

                document.Add(new DiamondTable(new List<PdfPCell>()
                {
                     new DiamondCell("Linea:", Element.ALIGN_LEFT, FontType.NormalBold,ShowBorderDebug),
                     new DiamondCell(_etiquetaEntrada.linea_fletera, Element.ALIGN_LEFT, FontType.Normal,ShowBorderDebug),
                     new DiamondCell("F.B.No.:", Element.ALIGN_RIGHT, FontType.NormalBold,ShowBorderDebug),
                     new DiamondCell(_etiquetaEntrada.guia, Element.ALIGN_RIGHT, FontType.Normal,ShowBorderDebug)
                }));

                document.Add(new DiamondTable(new List<PdfPCell>()
                {
                     new DiamondCell("Fecha:", Element.ALIGN_LEFT, FontType.NormalBold,ShowBorderDebug),
                     new DiamondCell(_etiquetaEntrada.fecha_entrada, Element.ALIGN_LEFT, FontType.Normal,ShowBorderDebug)
                }, new float[] { 67, 524 }));

                var ls_Obs = new List<PdfPCell>();
                var dcObs = new DiamondCell(_etiquetaEntrada.observaciones, Element.ALIGN_LEFT, FontType.Normal, ShowBorderDebug);
                dcObs.PaddingLeft = 0;
                ls_Obs.Add(dcObs);
                var tblObs = new DiamondTable(ls_Obs);
                document.Add(tblObs);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                document.Close();
                file.Close();
            }

            return file.Name;
        }
        
    }
}
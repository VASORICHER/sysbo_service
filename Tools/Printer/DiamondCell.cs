﻿using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SYSBO.Tools.Printer
{
    public class DiamondCell : PdfPCell
    {
        private float heigth = 16f;
        //============================CREATE CELL WITH TEXT LIKE PARAGRAPH=====================
        public DiamondCell(string _text, int _aligment, Font _fontType, BaseColor _backColor, bool _border = true)
            : base(new Paragraph(_text, _fontType) { Alignment = _aligment })
        {
            if (!_border)
                this.border = 0;
            this.backgroundColor = _backColor;
            this.HorizontalAlignment = _aligment;
            this.FixedHeight = heigth;
        }
        public DiamondCell(string _text, int _aligment, Font _fontType, bool _border = true)
            : base(new Paragraph(_text, _fontType) { Alignment = _aligment })
        {
            if (!_border)
                this.border = 0;
            if (_fontType != FontType.TituloBold)
                this.FixedHeight = heigth;
            this.HorizontalAlignment = _aligment;

        }
        public DiamondCell(string _text, int _aligment, Font _fontType)
        : base(new Paragraph(_text, _fontType) { Alignment = _aligment })
        {
            this.HorizontalAlignment = _aligment;
            this.FixedHeight = heigth;
        }
        public DiamondCell(string _text, int _aligment)
            : base(new Paragraph(_text, FontType.Normal) { Alignment = _aligment })
        {
            this.HorizontalAlignment = _aligment;
            this.FixedHeight = heigth;
        }
        //============================CREATE CELL WITH IMAGE=========================================
        /// <summary>
        /// Genera una celda con una imagen
        /// </summary>
        /// <param name="scaleAbsolute">scaleAbsolute toma dos valores de tipo float</param>
        public DiamondCell(string pathImage, int _border, float[] scaleAbsolute, int _aligment)
        {
            this.border = _border;
            iTextSharp.text.Image image = Image.GetInstance(pathImage);
            image.ScaleAbsolute(scaleAbsolute[0], scaleAbsolute[1]);
            image.Alignment = _aligment;
            this.AddElement(image);
        }
        /// <summary>
        /// Genera una celda con una imagen
        /// </summary>
        /// <param name="scaleAbsolute">scaleAbsolute toma dos valores de tipo float</param>
        public DiamondCell(string pathImage, int _border, float[] scaleAbsolute)
        {
            this.border = _border;
            iTextSharp.text.Image image = Image.GetInstance(pathImage);
            image.ScaleAbsolute(scaleAbsolute[0], scaleAbsolute[1]);
            image.Alignment = Element.ALIGN_LEFT;
            this.AddElement(image);

        }
        /// <summary>
        /// Genera una celda con una imagen
        /// </summary>
        /// <param name="scalePorcent">scalePorcent toma un valor de tipo float</param>
        public DiamondCell(string pathImage, int _border, float scalePorcent, int _aligment)
        {
            this.border = _border;
            iTextSharp.text.Image image = Image.GetInstance(pathImage);
            image.ScalePercent(scalePorcent);
            image.Alignment = _aligment;
            this.AddElement(image);
        }
        /// <summary>
        /// Genera una celda con una imagen
        /// </summary>
        /// <param name="scalePorcent">scalePorcent toma un valor de tipo float</param>
        public DiamondCell(string pathImage, int _border, float scalePorcent)
        {
            this.border = _border;
            iTextSharp.text.Image image = Image.GetInstance(pathImage);
            image.ScalePercent(scalePorcent);
            image.Alignment = Element.ALIGN_LEFT;
            this.AddElement(image);
        }
        public DiamondCell(string pathImage) :
            base(iTextSharp.text.Image.GetInstance(pathImage))
        {
            this.border = 0;
        }
        public DiamondCell(PdfPTable _pdfPTable) : base(_pdfPTable) { }
    }
}
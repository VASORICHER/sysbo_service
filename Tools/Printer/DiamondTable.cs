﻿using System.Collections.Generic;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SYSBO.Tools.Printer
{
    public class DiamondTable : PdfPTable
    {
        public DiamondTable(IList<PdfPCell> _cellsOnTable, float[] _widthsOfCell, int _spacingBefore = 0)
                : base(_cellsOnTable.Count)
        {
            this.SetWidthPercentage(_widthsOfCell, PageSize.A4);
            this.SpacingBefore = _spacingBefore;
            foreach (var item in _cellsOnTable)
                this.AddCell(item);
        }
        public DiamondTable(IList<PdfPCell> _cellsOnTable, int _spacingBefore = 0)
                : base(_cellsOnTable.Count)
        {
            var _widthsOfCell = new float[_cellsOnTable.Count];
            for (int i = 0; i < _widthsOfCell.Length; i++)
                _widthsOfCell[i] = 588 / _widthsOfCell.Length;

            this.SetWidthPercentage(_widthsOfCell, PageSize.A4);
            this.SpacingBefore = _spacingBefore;
            foreach (var item in _cellsOnTable)
                this.AddCell(item);
        }
    }
}
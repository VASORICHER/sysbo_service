﻿using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SYSBO.Tools.Printer
{
    public class FontType
    {
        public static readonly Font Titulo;
        public static readonly Font TituloBold;

        public static readonly Font Subtitulo;
        public static readonly Font SubtituloBold;

        public static readonly Font Normal;
        public static readonly Font NormalBold;

        //public static readonly Font BarCodeFont;
        static FontType()
        {
            //DECLARACION DE FUENTES CON SUS TAMANOS Y GROSORES ETC
            BaseFont baseFontHelvetica = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
            Barcode39 barcode = new Barcode39();

            Titulo = new Font(baseFontHelvetica, 13);//.DEFAULTSIZE);
            TituloBold = new Font(baseFontHelvetica, 13, Font.BOLD);

            Subtitulo = new Font(baseFontHelvetica, 4);
            SubtituloBold = new Font(baseFontHelvetica, 4, Font.BOLD);

            Normal = new Font(baseFontHelvetica, 10);
            NormalBold = new Font(baseFontHelvetica, 10, Font.BOLD);

            ////------
            //var d = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/../..", "mrvcode39extma.ttf");
            //BaseFont baseBarCode = BaseFont.CreateFont(d, BaseFont.CP1250, BaseFont.EMBEDDED);
            //BarCodeFont = new Font(baseBarCode, 30, Font.NORMAL);
        }
    }
}
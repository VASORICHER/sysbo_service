﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SYSBO
{
    public class cl_valida_dato
    {
        public bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            if (DateTime.TryParse(txtDate, out tempDate))
            {
                return true;
            }
            return false;
        }
        public bool IsNumeric(String str)
        {
            double myNum = 0;
            if (Double.TryParse(str, out myNum))
            {
                return true;
            }
            return false;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;
using System.IO;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Mail;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SYSBO
{
    public class util
    {
         public static string gral_correo_tecnico = "carmen.torres@ericher.com";
        //public static string garl_bitacora = Directory.GetCurrentDirectory() + @"\WBito.txt";
        //public static string garl_bitacora = @"q:\WBito.txt";//Directory.GetCurrentDirectory() + @"\WBito.txt";
        public static string gral_ruta_bitacora = @"C:\SYSBO\";
        public static string gral_nombre_bitacora = "SYSBO.txt";
        //c$\inetpub\ServiciosWeb\wms

        public static string gral_correo_deEnvio = "desarrollo@ericher.com";
        public static string gral_password_correo_deEnvio = "s15d3S@";
        public static string gral_administrador_correo_deEnvio = "Ermilo E Richer Agencia Aduanal";

        public bool bitacora(string texto, int email_sino)
        {
            bool lreturn;
            lreturn = true;

            string archivo, email_tecnico;
            archivo = gral_ruta_bitacora + gral_nombre_bitacora;
            email_tecnico = gral_correo_tecnico;

            bool exists = System.IO.Directory.Exists(gral_ruta_bitacora);
            if (!exists)
                System.IO.Directory.CreateDirectory(gral_ruta_bitacora);

            try
            {

                if (File.Exists(archivo))
                {
                    FileInfo finfo = new FileInfo(archivo);
                    long tamano = finfo.Length;
                    if (tamano > 52428800)
                    {
                        string anno, mes, dia, hora, min, seg;
                        DateTime fch = DateTime.Now;
                        anno = Convert.ToString(fch.Year);
                        if (fch.Month < 10)
                            mes = "0" + Convert.ToString(fch.Month);
                        else
                            mes = Convert.ToString(fch.Month);

                        if (fch.Day < 10)
                            dia = "0" + Convert.ToString(fch.Day);
                        else
                            dia = Convert.ToString(fch.Day);

                        if (fch.Hour < 10)
                            hora = "0" + Convert.ToString(fch.Hour);
                        else
                            hora = Convert.ToString(fch.Hour);

                        if (fch.Minute < 10)
                            min = "0" + Convert.ToString(fch.Minute);
                        else
                            min = Convert.ToString(fch.Minute);

                        if (fch.Second < 10)
                            seg = "0" + Convert.ToString(fch.Second);
                        else
                            seg = Convert.ToString(fch.Second);

                        char delimiterchars = '.';
                        string[] vectorRuta = archivo.Split(delimiterchars);
                        string archivo2 = vectorRuta[0];
                        delimiterchars = '\\';
                        string[] vectorArchivo = archivo2.Split(delimiterchars);
                        string soloArchivo = vectorArchivo[vectorArchivo.Length - 1];
                        string archivoNuevo = soloArchivo + "_" + anno + mes + dia + hora + min + seg + ".txt";
                        string soloRuta = vectorArchivo[0] + @"\";

                        if (File.Exists(soloRuta + archivoNuevo))
                        {
                            System.IO.File.Delete(soloRuta + archivoNuevo);
                        }
                        System.IO.File.Move(archivo, soloRuta + archivoNuevo);
                    }
                }

                if (!File.Exists(archivo))
                {
                    System.IO.StreamWriter swc;
                    swc = File.CreateText(archivo);
                    swc.WriteLine(texto);
                    swc.Close();
                }
                else
                {
                    System.IO.StreamWriter swr;
                    swr = File.AppendText(archivo);
                    swr.WriteLine(texto);
                    swr.Close();
                }

                if (email_sino == 1)
                {
                    string para, cc, bcc, titulo, mensaje;
                    string[] archivos = new string[10];
                    archivos[0] = "uno";
                    para = email_tecnico;
                    cc = "";
                    bcc = "";
                    titulo = "Alerta aplicacion WMS";
                    mensaje = texto;
                    envio_email(para, cc, bcc, titulo, mensaje, archivos, "N");
                }

            }
            catch (Exception e)
            {
                lreturn = false;
            }
            //finally
            //{            
            //} 

            return lreturn;
        }
        public string conexion_internet()
        {
            bool networkUp = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            DateTime fch = DateTime.Now;
            if (networkUp != true)
            {
                bitacora(Convert.ToString(fch) + "   [mix.conexion_internet]. Error de conexion a internet", 0);
                return Convert.ToString(fch) + "   [mix.conexion_internet]. Error de conexion a internet";
            }
            else
            {
                return "";
            }

        }
        public string conexion_servidor()
        {
            DateTime fch = DateTime.Now;
            Ping pingSender = new Ping();
            PingReply reply = pingSender.Send("192.168.1.11");

            if (reply.Status != IPStatus.Success)
            {
                //Console.WriteLine("Address: {0}", reply.Address.ToString());
                //Console.WriteLine("RoundTrip time: {0}", reply.RoundtripTime);
                //Console.WriteLine("Time to live: {0}", reply.Options.Ttl);
                //Console.WriteLine("Don't fragment: {0}", reply.Options.DontFragment);
                //Console.WriteLine("Buffer size: {0}", reply.Buffer.Length);
                bitacora(Convert.ToString(fch) + "   [mix.conexion_servidor]. No hay conexión al servidor SQL", 0);
                return Convert.ToString(fch) + "   [mix.conexion_servidor]. No hay conexión al servidor SQL";
            }
            else
            {
                return "";
            }
        }
        public string envio_email(String para, string concop, string bcc, string titulo, string mensaje, string[] archivos, string adjunta_archivo)
        {
            DateTime fch = DateTime.Now;
            MailMessage correo = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            int cont = 0;
            string valor_regresa = "";
            int n;
            string envia = "";
            string correo_envio, correo_password, administrador;

            administrador = gral_administrador_correo_deEnvio;
            correo_envio = gral_correo_deEnvio;
            correo_password = gral_password_correo_deEnvio;

            try
            {
                if (adjunta_archivo == "S") // ' N es para cuando solo se envian correos como alertas
                {
                    envia = "si";
                    ArrayList adjuntos = new ArrayList();
                    for (n = 0; n < archivos.Length - 1; n++)
                    {
                        if (string.IsNullOrEmpty(archivos[n]) == false)
                        {
                            if (File.Exists(archivos[n]) == false)
                            {
                                bitacora(Convert.ToString(fch) + "   [mix.envio_email]. Error archivo NO existe array(" + Convert.ToString(n) + "): " + archivos[n] + ", " + titulo + ", para " + para, 0);
                                valor_regresa = Convert.ToString(fch) + "   [mix.envio_email]. Error archivo NO existe array(" + Convert.ToString(n) + "): " + archivos[n] + ", " + titulo + ", para " + para;
                                correo.Attachments.Dispose();
                                return valor_regresa;
                            }
                            FileInfo propiedades = new FileInfo(archivos[n]);
                            if (propiedades.Length > 0)
                            {
                                correo.Attachments.Add(new Attachment(archivos[n]));
                                cont++;
                            }
                            else
                            {
                                bitacora(Convert.ToString(fch) + "   [mix.envio_email]. Error archivo tamaño=" + Convert.ToString(propiedades.Length) + " array(" + Convert.ToString(n) + "): " + archivos[n] + ", " + titulo + ", para " + para, 0);
                                valor_regresa = Convert.ToString(fch) + "   [mix.envio_email]. Error archivo tamaño=" + Convert.ToString(propiedades.Length) + " array(" + Convert.ToString(n) + "): " + archivos[n] + ", " + titulo + ", para " + para;
                                correo.Attachments.Dispose();
                                return valor_regresa;
                            }
                        }
                        else
                        {
                            if (n == 0)
                            { envia = "no"; }
                            n = archivos.Length;
                        }
                    }
                    if (cont > 0)
                    { envia = "si"; }
                    else
                    { envia = "no"; }
                }
                else
                { envia = "si"; }
                if (envia == "si")
                {
                    MailAddress from = new MailAddress(correo_envio, administrador, System.Text.Encoding.UTF8);

                    char delimiterchars = ';';
                    string[] vector = para.Split(delimiterchars);
                    for (n = 0; n <= vector.Length - 1; n++)
                    {
                        //MessageBox.Show(vector[n], "correos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (vector[n].Length > 0)
                        { correo.To.Add(vector[n]); }
                    }
                    string[] vector_cc = concop.Split(delimiterchars);
                    for (n = 0; n <= vector_cc.Length - 1; n++)
                    {
                        if (vector_cc[n].Length > 0)
                        { correo.CC.Add(vector_cc[n]); }
                    }
                    string[] vector_bcc = bcc.Split(delimiterchars);
                    for (n = 0; n <= vector_bcc.Length - 1; n++)
                    {
                        if (vector_bcc[n].Length > 0)
                        { correo.Bcc.Add(vector_bcc[n]); }
                    }
                    correo.From = from;
                    correo.Subject = titulo;
                    correo.Body = mensaje + (char)13 + (char)13 + (char)13 + (char)13 + "----------------------------------------------------------------------" + (char)13 + "Por favor, no responda este correo; esta es una cuenta no monitoreada.";
                    correo.IsBodyHtml = false;
                    correo.Priority = MailPriority.High;
                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(correo_envio, correo_password);
                    valor_regresa = conexion_internet();
                    if (valor_regresa.Length <= 0)
                    {
                        try
                        {
                            smtp.Send(correo);
                        }
                        catch (Exception e)
                        {
                            bitacora(Convert.ToString(fch) + "   [mix.envio_email]. Error al enviar a correo-e, " + titulo + ", para " + para + ".   " + e.Message, 0);
                            valor_regresa = Convert.ToString(fch) + "   [mix.envio_email]. Error al enviar a correo-e, " + titulo + ", para " + para + ".   " + e.Message;
                        }
                        finally
                        { }
                    }
                }
                else
                {
                    bitacora(Convert.ToString(fch) + "   [mix.envio_email].  No se envía correo-e no pasa las validaciones, " + titulo + ", para " + para, 0);
                    valor_regresa = Convert.ToString(fch) + "   [mix.envio_email].  No se envía correo-e no pasa las validaciones, " + titulo + ", para " + para;
                }
            }
            catch (Exception e)
            {
                bitacora(Convert.ToString(fch) + "   [mix.envio_email]. Error general al enviar a correo-e, " + titulo + ", para " + para + ".   " + e.Message, 0);
                valor_regresa = Convert.ToString(fch) + "   [mix.envio_email]. Error general al enviar a correo-e, " + titulo + ", para " + para + ".   " + e.Message;
            }
            finally
            { }
            correo.Attachments.Dispose();
            return valor_regresa;
        }
        public string obten_ip()
        {
            String strHostName = string.Empty;
            strHostName = Dns.GetHostName();
            //Console.WriteLine("Local Machine's Host Name: " + strHostName);
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            /*
            return addr[3].ToString();
            for (int i = 0; i < addr.Length; i++)
            {
                lst_error.Items.Add(addr[i].ToString());
            }
            */
            return strHostName + ":" + addr[addr.Length - 1].ToString();
            //return strHostName;
        }
    }
}